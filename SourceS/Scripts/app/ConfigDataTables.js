﻿$(document).ready(function () {
    var table = $('#example').DataTable({
        responsive: true,
        autoWidth: false,
        "language": {
            "lengthMenu": "Mostrar _MENU_ registros",
            "zeroRecords": "Sin registros.",
            "info": "Pagina _PAGE_ de _PAGES_",
            "search": "",
            "infoEmpty": "Sin resultados.",
            "infoFiltered": "(filtrado de _MAX_ registro totales)",
            searchPlaceholder: "Buscar registros",
            fixedColumns: {
                leftColumns: 2
            },
            paginate: {
                first: 'Primera p�gina',
                last: '�ltima p�gina',
                previous: 'Anterior',
                next: 'Siguiente'
            },
        }
    });

    // Add event listener for opening and closing details
    $('#example tbody').on('click', 'td.details-control', function () {
        console.log("Paso tabla")
        var tr = $(this).closest('tr');
        var row = table.row(tr);

        if (row.child.isShown()) {
            console.log("Paso hide");
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            console.log("Paso aqui");
            // Open this row
            row.child(format(row.data())).show();
            tr.addClass('shown');
        }
    });

});