﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SourceS.Models
{
    public  abstract class Persona
    {    
        [Key]
        [ForeignKey("ApplicationUser")]
        public string personaID { get; set; }
        public virtual ApplicationUser ApplicationUser { get; set; }
        [Required(ErrorMessage =("Este campo es requerido"))]
        [RegularExpression(@"^[a-zA-Z-Ññ\s]+$", ErrorMessage = "Solo se permiten letras  en estos campo")]
        [Display(Name = "Nombre")]
        [MaxLength(60)]
        public string Nombre { get; set; }
        [Required(ErrorMessage =("Este campo es requerido"))]
        [RegularExpression(@"^[a-zA-Z-Ññ\s]+$", ErrorMessage = "Solo se permiten letras  en estos campo")]
        [Display(Name = "Apellido paterno")]
        [MaxLength(60)]
        public string ApellidoPaterno { get; set; }
        [Required(ErrorMessage = ("Este campo es requerido"))]
        [RegularExpression(@"^[a-zA-Z-Ññ\s]+$", ErrorMessage = "Solo se permiten letras  en estos campo")]
        [Display(Name = "Apellido materno")]
        [MaxLength(60)]
        public string ApellidoMaterno { get; set; }
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Número telefonico no válido")]
        [MaxLength(10)]
        [Display(Name = "Teléfono celular")]
        public string Celular { get; set; }
    }
}