﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SourceS.Models.ViewModels
{
    public class HistoricoViewModel
    {
        [Required(ErrorMessage = ("Campo Fecha requerido"))]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime? Fecha { get; set; }
        [Required(ErrorMessage = ("Campo Hora requerido"))]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:HH:mm}")]
        public DateTime? Hora { get; set; }
        public int Gasolinera { get; set; }
        public ProductoFormularioDto[] Productos { get; set; }
    }
}