﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SourceS.Models.ViewModels
{
    public class ProductoFormularioDto
    {

        public string name { get; set; }
        [Required(ErrorMessage = ("El campo precio es requerido"))]
        [RegularExpression(@"^\d{2}\.\d{2}$", ErrorMessage = "El precio debe contener 2 digitos y 2 decimales.")]
        public string price { get; set; }
    }
}