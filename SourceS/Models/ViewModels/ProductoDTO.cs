﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SourceS.Models.ViewModels
{
    public class ProductoDTO
    {
        [Required(ErrorMessage = ("Este campo es requerido"))]
        [Display(Name = ("Nombre"))]
        [StringLength(50, MinimumLength = 3, ErrorMessage = ("Este campo debe tener como minimo 3 caracteres y maximo 50"))]
        public string Nombre { get; set; }
        [Required(ErrorMessage = ("Este campo es requerido"))]
        [RegularExpression(@"\d{2}\.\d{2}$", ErrorMessage = "Este campo es requerido con el siguiente formato 2 digitos y 2 decimales.")]
        public string Precio { get; set; }
        [DataType(DataType.Date)]
        [Required, DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime RowVersion { get; set; }
        public bool IsDeleted { get; set; }
    }
}