﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SourceS.Models.ViewModels.ApagadoDevice
{
    public class ProgramaDto
    {
        public string ip { get; set; }
        public int identificador { get; set; }
        public string fecha { get; set; }
        public string hora { get; set; }
        public string precio { get; set; }
        public string estatus { get; set; }
        public string comando { get; set; }
        public int baudio { get; set; }
        public string apagado { get; set; }
    }
}