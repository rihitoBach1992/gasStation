﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SourceS.Models.ViewModels
{
    public class DetailHistoricoVM
    {
        public int historicoID { get; set; }
        [DataType(DataType.Date)]
        public DateTime fecha { get; set; }
        public string hora { get; set; }
        [DataType(DataType.Currency)]
        public double precio { get; set; }
        public bool estatus { get; set; }
        public string nombreProducto { get; set; }
    }
}