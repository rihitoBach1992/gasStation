﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SourceS.Models.ViewModels.FormularioMovil
{
    public class HistorialesJSON
    {
        [DataType(DataType.Time)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:HH:mm}")]
        public DateTime hora { get; set; }
        public DateTime fecha { get; set; }
        public List<string> precios { get; set; }
        public List<String> nombres { get; set; }
        public string tracks { get; set; }
    }
}