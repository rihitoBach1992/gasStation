﻿using SourceS.Models.CustomValidation;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SourceS.Models.FormularioAdministrador
{
    public class DispositivoDTO
    {
        [Required(ErrorMessage = ("Este campo es requerido"))]
        [Display(Name = ("Dirección IP"))]
        [IpAddress]
        public string ip { get; set; }
        [Required(ErrorMessage = ("Este campo es requerido"))]
        [Display(Name = ("Nombre"))]
        [StringLength(25, MinimumLength = 3, ErrorMessage = ("Este campo debe tener como minimo 3 caracteres y maximo 25"))]
        public string nombre { get; set; }
        [Required(ErrorMessage =("Este campo es requerido"))]
        public CantidadProductos CantidadProductos { get; set; }
    }

    public enum CantidadProductos
    {
        Uno,
        Dos,
        Tres,
        Cuatro,
        Cinco
    }
}