﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SourceS.Models
{
    [Table("dbo.Gasolineras")]
    public class Gasolinera
    {

        public Gasolinera()
        {
            this.Dispositivos = new HashSet<Dispositivo>();
            this.Productos = new HashSet<Producto>();
        }

        [Key]
        [JsonProperty(Order = 1)]
        public int gasolineraID { get; set; }
        [Column(Order = 2)]
        [JsonProperty(Order = 2)]
        public string identificador { get; set; }
        [Required(ErrorMessage =("Este campo es requerido")), MaxLength(100)]
        [RegularExpression(@"^[a-zA-Z-Ññ\s]+$", ErrorMessage = "Solo se permiten letras  en estos campo")]
        [Display(Name = "Ciudad")]
        [JsonProperty(Order = 3)]
        public string ciudad { get; set; }
        [Required(ErrorMessage = ("Este campo es requerido")), MaxLength(100)]
        [RegularExpression(@"^[a-zA-Z-Ññ\s]+$", ErrorMessage = "Solo se permiten letras  en estos campo")]
        [Display(Name = "Municipio")]
        [JsonProperty(Order = 4)]
        public string municipio { get; set; }
        [Required(ErrorMessage = ("Este campo es requerido")), MaxLength(100)]
        [RegularExpression(@"^[a-zA-Z-Ññ\s]+$", ErrorMessage = "Solo se permiten letras  en estos campo")]
        [Display(Name = "Estado")]
        [JsonProperty(Order = 5)]
        public string estado { get; set; }
        [Required(ErrorMessage = ("Este campo es requerido")), MaxLength(100)]
        [Display(Name = "Colonia")]
        [JsonProperty(Order = 6)]
        public string colonia { get; set; }
        [Required(ErrorMessage = ("Este campo es requerido")), MaxLength(100)]
        [Display(Name = "Calle")]
        [JsonProperty(Order = 7)]
        public string calle { get; set; }
        [JsonProperty(Order = 8)]
        public virtual ICollection<Dispositivo> Dispositivos{ get; set; }
        [JsonProperty(Order = 9)]
        public virtual ICollection<Producto> Productos { get; set; }
        [ForeignKey("Dueño")]
        public string idDueño { get; set; }
        public virtual Dueño Dueño { get; set; }
    }
}