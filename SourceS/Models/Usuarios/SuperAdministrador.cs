﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SourceS.Models.Usuarios
{
    [Table("dbo.SuperAdministradores")]
    public class SuperAdministrador:Persona
    {
        public SuperAdministrador()
        {
            this.Administradores = new HashSet<Administrador>();
        }

        [DataType(DataType.Date)]
        public DateTime Creado { get; set; }
        public virtual ICollection<Administrador> Administradores { get; set; }
    }
}