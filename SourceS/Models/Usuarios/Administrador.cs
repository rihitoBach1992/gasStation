﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SourceS.Models.Usuarios
{
    [Table("dbo.Administradores")]
    public class Administrador:Persona
    {
        public Administrador()
        {
            this.Dueños = new HashSet<Dueño>();
        }
        public bool Habilitado { get; set; }
        [DataType(DataType.Date)]
        public DateTime Creado { get; set; }
        public virtual ICollection<Dueño> Dueños { get; set; }
    }
}