﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration.Conventions;
using SourceS.Models.Helper;
using ADES.Models.Helpers;
using System.Linq;
using SourceS.Models.Usuarios;

namespace SourceS.Models
{
    // Puede agregar datos del perfil del usuario agregando más propiedades a la clase ApplicationUser. Para más información, visite http://go.microsoft.com/fwlink/?LinkID=317594.
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Tenga en cuenta que el valor de authenticationType debe coincidir con el definido en CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Agregar aquí notificaciones personalizadas de usuario
            return userIdentity;
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public  virtual DbSet<SuperAdministrador> SuperAdministradores { get; set; }
        public virtual DbSet<Administrador> Administradores { get; set; }
        public virtual DbSet<Dueño> Dueños { get; set; }
        public virtual DbSet<Gasolinera> Gasolineras {get;set;}
        public virtual DbSet<Producto> Productos { get; set; }
        public virtual DbSet<Dispositivo> Dispositivos { get; set; }
        public virtual DbSet<Historico> Historicos { get; set; }




        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

            base.OnModelCreating(modelBuilder);
            var conv = new AttributeToTableAnnotationConvention<SoftDeleteAttribute, string>(
               "SoftDeleteColumnName",
               (type, attributes) => attributes.Single().ColumnName);

            modelBuilder.Conventions.Add(conv);
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        #region IdentityManager
        public class IdentityManager
        {
            public bool RoleExists(string name)
            {
                var rm = new RoleManager<IdentityRole>(
                    new RoleStore<IdentityRole>(new ApplicationDbContext()));
                return rm.RoleExists(name);
            }


            public bool CreateRole(string name)
            {
                var rm = new RoleManager<IdentityRole>(
                    new RoleStore<IdentityRole>(new ApplicationDbContext()));
                var idResult = rm.Create(new IdentityRole(name));
                return idResult.Succeeded;
            }


            public bool CreateUser(ApplicationUser user, string password)
            {
                var um = new UserManager<ApplicationUser>(
                    new UserStore<ApplicationUser>(new ApplicationDbContext()));
                var idResult = um.Create(user, password);
                return idResult.Succeeded;
            }


            public bool AddUserToRole(string userId, string roleName)
            {
                var um = new UserManager<ApplicationUser>(
                    new UserStore<ApplicationUser>(new ApplicationDbContext()));
                var idResult = um.AddToRole(userId, roleName);
                return idResult.Succeeded;
            }
        }
        #endregion

        public class SourceConfiguration : DbConfiguration
        {
            public SourceConfiguration()
            {
                AddInterceptor(new SoftDeleteInterceptor());
            }
        }
    }
}