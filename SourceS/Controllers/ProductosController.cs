﻿using System.Data.Entity;
using System.Net;
using System.Web.Mvc;
using SourceS.Models;

namespace SourceS.Controllers
{
    [Authorize(Roles =("Admin,Administrador"))]
    public class ProductosController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Productos/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Producto producto = db.Productos.Find(id);
            if (producto == null)
            {
                return HttpNotFound();
            }
            return View(producto);
        }

        // GET: Productos/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Productos/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "productoID,Nombre,Moonto,RowVersion")] Producto producto)
        {
            if (ModelState.IsValid)
            {
                db.Productos.Add(producto);
                db.SaveChanges();
                return RedirectToAction("Create");
            }

            return View(producto);
        }


        // GET: Productos/Edit/5
        [HttpGet]
        public ActionResult Edit(int? id,string Andromeda)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            

            Producto producto = db.Productos.Find(id);
            ViewBag.Androide = Andromeda;
            if (producto == null)
            {
                return HttpNotFound();
            }
            return PartialView("_Edit",producto);
        }

        // POST: Productos/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "productoID,Nombre,Precio,RowVersion,GasolineraId,Letra")] Producto producto,string Andromeda)
        {
            if (ModelState.IsValid)
            {
                producto.Gasolinera = db.Gasolineras.Find(producto.GasolineraId);

                db.Entry(producto).State = EntityState.Modified;
                db.SaveChanges();

                return Json(new {success = true });
            }
            return PartialView("_Edit",producto);
        }


        // GET: Productos/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Producto producto = db.Productos.Find(id);
            if (producto == null)
            {
                return HttpNotFound();
            }
            return View(producto);
        }

        // POST: Productos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Producto producto = db.Productos.Find(id);
            db.Productos.Remove(producto);
            db.SaveChanges();
            return RedirectToAction("Delete");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
