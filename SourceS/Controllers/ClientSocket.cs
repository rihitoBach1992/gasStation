﻿using SourceS.Models.ViewModels.ApagadoDevice;
using System;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Web.Script.Serialization;

namespace SourceS.Controllers
{
    public class ClientSocket
    {
            private TcpClient clientSocket;
            private int port = 8090;
  
            public void StartServer(ProgramaDto historico)
            {
                var t = new Thread(() => ClientThreadStart(historico));
                t.IsBackground = true;
                t.Start();
            }

            public void ClientThreadStart(ProgramaDto historico)
            {
                //Open Socket
                const int bytesize = 1024;

                //Get Historial Object
                var historial = historico;
                JavaScriptSerializer ser = new JavaScriptSerializer();
                var json = ser.Serialize(historial);
                var messageBytes = Encoding.UTF8.GetBytes(json);

                try
                {
                    clientSocket = new TcpClient();
                    clientSocket.Connect(historial.ip, port);
                    clientSocket.ReceiveBufferSize = 1024;
                    clientSocket.SendBufferSize = 1024;

                    var stream = clientSocket.GetStream();
                    stream.Write(messageBytes, 0, messageBytes.Length);
                    stream.Flush();// Write the bytes  
                    messageBytes = new byte[bytesize]; // Clear the message   


                    //clientSocket.ReceiveTimeout = 60000;
                    // Receive the stream of bytes  
                    stream.Read(messageBytes, 0, messageBytes.Length);
                    var result = Encoding.UTF8.GetString(messageBytes);
                    result = result.Slice(0, 8);
                    //stream.Dispose();
                    stream.Close();
                    OnStopServer();

                }
                catch (Exception e)
                {
                    Console.WriteLine("Exception en el Client Socket ASP.NET" + e.Message + e.StackTrace);
                    OnStopServer();
                }
            }


            public void OnStopServer()
            {
                clientSocket.Close();
            }
        }
        public static class Extensions
        {
            /// <summary>
            /// Get the string slice between the two indexes.
            /// Inclusive for start index, exclusive for end index.
            /// </summary>
            public static string Slice(this string source, int start, int end)
            {
                if (end < 0) // Keep this for negative end support
                {
                    end = source.Length + end;
                }
                int len = end - start;               // Calculate length
                return source.Substring(start, len); // Return Substring of length
            }
        }
    }
