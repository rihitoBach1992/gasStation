﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using SourceS.Models;

namespace SourceS.Controllers.WebServices
{
    public class DueñosJSController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/DueñosJS/5
        [HttpPost]
        public IHttpActionResult CreateOwner(LoginViewModel login)
        {
            try
            {
                if(!ModelState.IsValid){

                    return BadRequest(ModelState);
                }

                if (string.IsNullOrEmpty(login.Email))
                {
                    ModelState.AddModelError("Email", "Este campo es requerido");
                    return BadRequest(ModelState);
                }

                var username = login.Email;
                var password = login.Password;

                UserStore<ApplicationUser> store = new UserStore<ApplicationUser>(db);
                UserManager<ApplicationUser> UserManager = new UserManager<ApplicationUser>(store);
                var user = UserManager.Find(username, password);

                if(user == null)
                {
                    ModelState.AddModelError("Error", "No existe el usuario en el sistema");

                    return BadRequest(ModelState);
                }

                return Ok(user);
            

            }
            catch (Exception e)
            {
                ModelState.AddModelError("Error", "No existe el usuario en el sistema");
                Debug.WriteLine("no se encontro el usuario");
                return BadRequest(ModelState);
            }
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool DueñoExists(string id)
        {
            return db.Dueños.Count(e => e.personaID == id) > 0;
        }
    }
}