﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using SourceS.Models;
using System.Threading.Tasks;
using SourceS.Models.ViewModels;

namespace SourceS.Controllers.WebServices
{
    [RoutePrefix("api/GasolinerasJS")]
    public class GasolinerasJSController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();



        // GET: api/GasolinerasJS/lista/1298319-982jks
        [Route("lista/{idOwner}")]
        public  async Task<List<Gasolinera>> GetGasolinerasbyID(string idOwner)
        {
            return await db.Dueños.Where(g => g.personaID == idOwner).Include(g => g.Gasolineras)
               .SelectMany(g => g.Gasolineras).ToListAsync();
        }

        [Route("Movil/{idOwner}")]
        public async Task<List<GasolinerasMovil>> GetGasolinerasMovil(string idOwner)
        {
            var gasolinerasMovil = await db.Dueños.Where(g => g.personaID == idOwner).Include(g => g.Gasolineras)
                .SelectMany(g => g.Gasolineras).Select(n => new GasolinerasMovil() { Id = n.gasolineraID, Identificador = n.identificador}).ToListAsync();

            return gasolinerasMovil;

        }

        // GET: api/GasolinerasJS/5
        [Route("gasolinera/{id}")]
        [ResponseType(typeof(Gasolinera))]
        public IHttpActionResult GetGasolinera(int id)
        {
            Gasolinera gasolinera = db.Gasolineras.Find(id);
            if (gasolinera == null)
            {
                return NotFound();
            }

            return Ok(gasolinera);
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}