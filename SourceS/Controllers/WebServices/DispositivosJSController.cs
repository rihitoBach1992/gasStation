﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using SourceS.Models;

namespace SourceS.Controllers.WebServices
{
    public class DispositivosJSController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/DispositivosJS
        public IQueryable<Dispositivo> GetDispositivos()
        {
            return db.Dispositivos;
        }

        // GET: api/DispositivosJS/5
        [ResponseType(typeof(Dispositivo))]
        public IHttpActionResult GetDispositivo(int id)
        {
            Dispositivo dispositivo = db.Dispositivos.Find(id);
            if (dispositivo == null) 
            {
                return NotFound();
            }

            return Ok(dispositivo);
        }

        // PUT: api/DispositivosJS/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutDispositivo(int id, Dispositivo dispositivo)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != dispositivo.dispositivoID)
            {
                return BadRequest();
            }

            db.Entry(dispositivo).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DispositivoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/DispositivosJS
        [ResponseType(typeof(Dispositivo))]
        public IHttpActionResult PostDispositivo(Dispositivo dispositivo)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Dispositivos.Add(dispositivo);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = dispositivo.dispositivoID }, dispositivo);
        }

        // DELETE: api/DispositivosJS/5
        [ResponseType(typeof(Dispositivo))]
        public IHttpActionResult DeleteDispositivo(int id)
        {
            Dispositivo dispositivo = db.Dispositivos.Find(id);
            if (dispositivo == null)
            {
                return NotFound();
            }

            db.Dispositivos.Remove(dispositivo);
            db.SaveChanges();

            return Ok(dispositivo);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool DispositivoExists(int id)
        {
            return db.Dispositivos.Count(e => e.dispositivoID == id) > 0;
        }
    }
}