﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using SourceS.Models;
using SourceS.Models.ViewModels;
using SourceS.Models.ViewModels.FormularioMovil;

namespace SourceS.Controllers.WebServices
{
    [RoutePrefix("api/HistoricosJS")]
    public class HistoricosJSController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/HistoricosJS/listabybID/12
        [Route("listabyID/{id}")]
        public IHttpActionResult GetHistoricosbyId(int id)
        {
            var gasolinera = db.Gasolineras.Find(id);
            List<HistorialesJSON> historialesJSON;
            var historiales = new List<HistorialesJSON>();
            var ahora = DateTime.Now.Date;
            if (gasolinera != null) { BadRequest("No tiene gasolinera"); }

            foreach (var dispositivo in gasolinera.Dispositivos)
            {
                historialesJSON =
                    db.Historicos.AsNoTracking()
                        .Where(d => d.dispositivoID == dispositivo.dispositivoID)
                        .GroupBy(g => g.hora)
                        .Select(
                            h =>
                                new HistorialesJSON()
                                {
                                    hora = h.Key,
                                    fecha = h.Select(s => s.fecha).FirstOrDefault(),
                                    precios = h.Select(s => s.precio).ToList(),
                                    nombres = h.Select(s => s.Producto.Nombre).ToList(),
                                    tracks = gasolinera.identificador
                                })
                        .ToList();

                historiales.AddRange(historialesJSON);
            }


            var json = new SchedulerHistorialJSON()
            {
                HistoricosToday = historiales.Where(g => g.fecha.Date == ahora).ToList(),
                Historicos = historiales
            };
            return Ok(json);
        }

        // GET: api/HistoricosJS/5
        [ResponseType(typeof(Historico))]
        public IHttpActionResult GetHistorico(int id)
        {
            Historico historico = db.Historicos.Find(id);
            if (historico == null)
            {
                return NotFound();
            }

            return Ok(historico);
        }


        [Route("programas/{nombre}")]
        public IHttpActionResult GetPrograms(string nombre)
        {
            var dispositivo = db.Dispositivos.FirstOrDefault(d => d.nombre == nombre);
            if (dispositivo == null) return BadRequest();

            var hora = DateTime.Now;
            var fecha = DateTime.Now.Date;

            var programas = db.Historicos.Where(p =>  p.dispositivoID == dispositivo.dispositivoID && p.enviado == false && p.hora == hora  && p.fecha == fecha);
           
            return Ok(programas);
        }


        public IHttpActionResult UpdateProgrma(int programaId)
        {
            var programa = db.Historicos.FirstOrDefault(p => p.historicoID == programaId);
            if (programa != null) programa.enviado = true;

            db.Entry(programa).State = EntityState.Modified;
            db.SaveChanges();

            return Ok(programa);
        }


        [Route("Estatus/{id}")]
        public IHttpActionResult PostProgramas(int id)
        {
            var programa = db.Historicos.FirstOrDefault(p => p.historicoID == id);
            if (programa != null) programa.estatus = true;

            db.Entry(programa).State = EntityState.Modified;
            db.SaveChanges();

      
            return Ok(programa);
        }




        // POST: api/HistoricosJS
        public IHttpActionResult PostHistorico(HistoricoViewModel historico)
        {

            if (!ModelState.IsValid) { return BadRequest(ModelState); }

            var gasolinera = db.Gasolineras.Find(historico.Gasolinera);
            if (gasolinera == null)
            {
                return BadRequest("No existe la gasolinera");
            }

            var dispositivo = gasolinera.Dispositivos.FirstOrDefault();
            if (dispositivo == null) return BadRequest("No tiene ningun dispositivo con la gasolinera");

                foreach (var productoGasolinera in gasolinera.Productos)
                {
                    //Configuraciones Productos
                    var producto = db.Productos.Find(productoGasolinera.ProductoId);
                    var historial = new Historico();
                    if (historico.Fecha != null) historial.fecha = historico.Fecha.Value;
                    if (historico.Hora != null) historial.hora = historico.Hora.Value;
                    historial.Dispositivo = dispositivo;
                    historial.dispositivoID = dispositivo.dispositivoID;
                    if (producto != null) historial.precio = GetPrecio(producto.Nombre, historico.Productos);
                    historial.Producto = productoGasolinera;
                    historial.ProductoId = productoGasolinera.ProductoId;
                    historial.estatus = false;
                    historial.enviado = false;
                    historial.IsDeleted = false;
                    db.Historicos.Add(historial);
                }
            try
            {
                db.SaveChanges();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

            return Ok(historico);
        }


        public string GetPrecio(string productoNombre, ProductoFormularioDto[] productosFormulario)
        {
            var precio = "";
            foreach (var productoFormulario in productosFormulario)
            {
                if (productoNombre.Equals(productoFormulario.name))
                {
                    precio = productoFormulario.price;
                }

            }
            return precio;
        }

        // DELETE: api/HistoricosJS/5
        [ResponseType(typeof(Historico))]
        public IHttpActionResult DeleteHistorico(int id)
        {
            Historico historico = db.Historicos.Find(id);
            if (historico == null)
            {
                return NotFound();
            }

            db.Historicos.Remove(historico);
            db.SaveChanges();

            return Ok(historico);
        }


        [Route("configuraciones/{id}")]
        public List<ConfiguracionMovil> GetConfigurationProducto(int id)
        {
            var configuraciones = db.Productos.Where(p => p.GasolineraId == id).Select(p => new ConfiguracionMovil() { name = p.Nombre, price = p.Precio }).ToList();
            return configuraciones;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool HistoricoExists(int id)
        {
            return db.Historicos.Count(e => e.historicoID == id) > 0;
        }
    }
}