﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SourceS.Models;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity;
using SourceS.Models.ViewModels;

namespace SourceS.Controllers
{
    public class HistoricosController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private ApplicationUserManager _userManager;

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        // GET: Historicos/Details/5
        [Authorize(Roles = ("Admin,Administrador,Usuario"))]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Historico historico = db.Historicos.Find(id);
            if (historico == null)
            {
                return HttpNotFound();
            }
            return PartialView("_Details", historico);
        }


        #region Create Programs
        // GET: Historicos/Create
        [Authorize(Roles = ("Usuario"))]
        public ActionResult Create(int? gasolineraId)
        {
            var userId = User.Identity.GetUserId();
            if (userId == null) { return HttpNotFound(); }

            var gasolinera = db.Gasolineras.Find(gasolineraId);
            if (gasolinera == null) throw new ArgumentNullException(nameof(gasolinera));

            //Gasolinera -> Dispositivo? -> ProductosGasolinera

            var historico = new HistoricoDinamicoDTO()
            {
                Fecha = DateTime.Now.Date,
                Hora = DateTime.Now,
                gasolinerId = gasolineraId,
                identificador = gasolinera.identificador,
                Productos = gasolinera.Productos.ToList()

            };
            return PartialView("_Create", historico);
        }


        // POST: Historicos/Create
        [HttpPost]
        [Authorize(Roles = ("Usuario"))]
        [ValidateAntiForgeryToken]
        public ActionResult Create(HistoricoDinamicoDTO programa)
        {
            var userId = User.Identity.GetUserId();
            if (userId == null) return HttpNotFound();


            if (ModelState.IsValid)
            {
                var gasolinera = db.Gasolineras.Find(programa.gasolinerId);
                if(gasolinera == null) { return PartialView("_Create",programa);}

                var dispositivo = gasolinera.Dispositivos.FirstOrDefault();
                foreach (var producto in programa.Productos)
                {
                    var createPrograma = new Historico()
                    {

                        fecha = programa.Fecha,
                        hora = programa.Hora,
                        enviado = false,
                        estatus = false,
                        IsDeleted = false,
                        ProductoId = producto.ProductoId,
                        Producto = producto,
                        precio = producto.Precio,
                        Dispositivo = dispositivo,
                        dispositivoID = dispositivo.dispositivoID

                    };
                    db.Historicos.Add(createPrograma);
                }

                try
                {
                    db.SaveChanges();
                    return Json(new {success = true});
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
            return PartialView("_Create",programa);
        }
        #endregion


        // GET: Historicos/Edit/5
        [Authorize(Roles = ("Usuario"))]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Historico historico = db.Historicos.Find(id);
            if (historico == null)
            {
                return HttpNotFound();
            }
            ViewBag.dispositivoID = new SelectList(db.Dispositivos, "dispositivoID", "ip", historico.dispositivoID);
            //ViewBag.productoGasolineraID = new SelectList(db.ProductosGasolinera, "productoGasolineraID", "productoGasolineraID", historico.productoGasolineraID);
            return PartialView("_Edit", historico);
        }

        // POST: Historicos/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = ("Usuario"))]
        public ActionResult Edit([Bind(Include = "historicoID,fecha,hora,precio,enviado,estatus,IsDeleted,dispositivoID,productoGasolineraID")] Historico historico)
        {
            if (ModelState.IsValid)
            {
                db.Entry(historico).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.dispositivoID = new SelectList(db.Dispositivos, "dispositivoID", "ip", historico.dispositivoID);
            // ViewBag.productoGasolineraID = new SelectList(db.ProductosGasolinera, "productoGasolineraID", "productoGasolineraID", historico.productoGasolineraID);
            return PartialView("_Edit", historico);
        }



        //?? 5 minutos antes de que se ejecute

        // GET: Historicos/Delete/5
        [Authorize(Roles = ("Admin"))]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Historico historico = db.Historicos.Find(id);
            if (historico == null)
            {
                return HttpNotFound();
            }
            return View(historico);
        }

        // POST: Historicos/Delete/5
        [Authorize(Roles = ("Admin"))]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Historico historico = db.Historicos.Find(id);
            db.Historicos.Remove(historico);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
