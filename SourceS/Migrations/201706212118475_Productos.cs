namespace SourceS.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Productos : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ProductosGasolinera", "GasolineraId", "dbo.Gasolineras");
            DropForeignKey("dbo.ProductosGasolinera", "ProductoId", "dbo.Productos");
            DropForeignKey("dbo.Historicos", "productoGasolineraID", "dbo.ProductosGasolinera");
            DropIndex("dbo.Historicos", new[] { "productoGasolineraID" });
            DropIndex("dbo.ProductosGasolinera", new[] { "GasolineraId" });
            DropIndex("dbo.ProductosGasolinera", new[] { "ProductoId" });
            AddColumn("dbo.Historicos", "ProductoId", c => c.Int());
            AddColumn("dbo.Productos", "GasolineraId", c => c.Int(nullable: false));
            CreateIndex("dbo.Historicos", "ProductoId");
            CreateIndex("dbo.Productos", "GasolineraId");
            AddForeignKey("dbo.Productos", "GasolineraId", "dbo.Gasolineras", "gasolineraID", cascadeDelete: true);
            AddForeignKey("dbo.Historicos", "ProductoId", "dbo.Productos", "ProductoId",cascadeDelete:false);
            DropColumn("dbo.Historicos", "productoGasolineraID");
            DropTable("dbo.ProductosGasolinera");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.ProductosGasolinera",
                c => new
                    {
                        ProductoGasolineraId = c.Int(nullable: false, identity: true),
                        GasolineraId = c.Int(nullable: false),
                        ProductoId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ProductoGasolineraId);
            
            AddColumn("dbo.Historicos", "productoGasolineraID", c => c.Int(nullable: false));
            DropForeignKey("dbo.Historicos", "ProductoId", "dbo.Productos");
            DropForeignKey("dbo.Productos", "GasolineraId", "dbo.Gasolineras");
            DropIndex("dbo.Productos", new[] { "GasolineraId" });
            DropIndex("dbo.Historicos", new[] { "ProductoId" });
            DropColumn("dbo.Productos", "GasolineraId");
            DropColumn("dbo.Historicos", "ProductoId");
            CreateIndex("dbo.ProductosGasolinera", "ProductoId");
            CreateIndex("dbo.ProductosGasolinera", "GasolineraId");
            CreateIndex("dbo.Historicos", "productoGasolineraID");
            AddForeignKey("dbo.Historicos", "productoGasolineraID", "dbo.ProductosGasolinera", "ProductoGasolineraId", cascadeDelete: true);
            AddForeignKey("dbo.ProductosGasolinera", "ProductoId", "dbo.Productos", "ProductoId", cascadeDelete: true);
            AddForeignKey("dbo.ProductosGasolinera", "GasolineraId", "dbo.Gasolineras", "gasolineraID", cascadeDelete: true);
        }
    }
}
